﻿namespace BlazorIdentity.Shared.Models
{
    public class Enquiry
    {
        public int Id { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int Customerid { get; set; }
    }
}
