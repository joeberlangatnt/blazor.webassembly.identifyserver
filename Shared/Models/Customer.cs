﻿using FluentValidation;
using System.Collections.Generic;

namespace BlazorIdentity.Shared.Models
{
    public class Customer
    {
        public Customer()
        {
            Enquiries = new HashSet<Enquiry>();
        }

        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }

        public virtual ICollection<Enquiry> Enquiries { get; set; }
    }

    public class CustomerValidaton : AbstractValidator<Customer>
    {
        public CustomerValidaton()
        {

            RuleFor(customer => customer.Firstname).NotNull().WithMessage("First Name is required").MaximumLength(50).WithMessage("First Name must be less than 50 characters");
            RuleFor(customer => customer.Lastname).NotNull().WithMessage("First Name is required").MaximumLength(50).WithMessage("Last Name must be less than 50 characters");
        }
    }
}
