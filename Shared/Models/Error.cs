﻿namespace BlazorIdentity.Shared.Models
{
    public class Error
    {
        public Error() 
        {
        }

        public Error(bool error) 
        {
            this.HasError = error;
        }

        public Error(bool error, string message)
        {
            this.HasError = error;
            this.Message = message;
        }

        public bool HasError { get; private set; } = false;

        public string Message { get; private set; } = "An unexpected error occured.";
    }
}
