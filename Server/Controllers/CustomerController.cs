﻿using System.Threading.Tasks;
using BlazorIdentity.Server.Services;
using BlazorIdentity.Shared.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BlazorIdentity.Server.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService _customerService)
        {
            this._customerService = _customerService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var customers = await _customerService.GetCustomers();

            return Ok(customers);
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetCustomer(int id) 
        {
            if (id <= 0)
            {
                //zeros are bad mmmmk
                return BadRequest();
            }

            var customer = await _customerService.GetCustomer(id);

            return Ok(customer);
        }

        [HttpGet("{searchTerm}")]
        public async Task<IActionResult> Search(string searchTerm) 
        {
            var customers = await this._customerService.Search(searchTerm);

            return Ok(customers);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id) 
        {
            if (id <= 0)
            {
                return BadRequest("Not a valid customer ID");
            }

            await _customerService.DeleteCustomer(id);

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Post(Customer customer)
        {
            await this._customerService.AddCustomer(customer);

            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Put(Customer customer)
        {
            if (customer.Id <= 0)
            {
                return BadRequest("Customer update requires an ID");
            }

            await this._customerService.UpdateCustomer(customer);

            return Ok();
        }
    }
}
