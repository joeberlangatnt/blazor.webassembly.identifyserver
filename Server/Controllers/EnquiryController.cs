﻿using BlazorIdentity.Server.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BlazorIdentity.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EnquiryController : ControllerBase
    {
        private readonly IEnquiryService _enquiryService;

        public EnquiryController(IEnquiryService _enquiryService) 
        {
            this._enquiryService = _enquiryService;
        }

        [HttpGet("{customerId:int}")]
        public async Task<IActionResult> GetEnquiriesByCustomerId(int customerId) 
        {
            if (customerId <= 0)
            {
                return BadRequest();
            }

            var enquiries = await _enquiryService.GetEnquiriesByCustomerId(customerId);

            return Ok(enquiries);
        }
    }
}
