﻿using BlazorIdentity.Server.Context;
using BlazorIdentity.Shared.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorIdentity.Server.Services
{
    //typical methods
    public class CustomerService : ICustomerService
    {
        private readonly TestContext _dbContext;

        public CustomerService(TestContext _testContext)
        {
            this._dbContext = _testContext;
        }

        public async Task AddCustomer(Customer customer)
        {
            await _dbContext.Customers.AddAsync(customer);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteCustomer(int id)
        {
            var customer = await _dbContext.Customers.FirstOrDefaultAsync(customer => customer.Id == id);
            _dbContext.Customers.Remove(customer);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<Customer> GetCustomer(int id)
        {
            var customer = await _dbContext.Customers.FirstOrDefaultAsync(c => c.Id == id);
            return customer;
        }

        public async Task<IEnumerable<Customer>> GetCustomers()
        {
            var customers = await _dbContext.Customers.ToListAsync();
            return customers;
        }

        public async Task<IEnumerable<Customer>> Search(string searchTerm)
        {
            var customers = await _dbContext.Customers.Where(customer => customer.Firstname.Contains(searchTerm) || customer.Lastname.Contains(searchTerm) || searchTerm.Contains(customer.Firstname) || searchTerm.Contains(customer.Lastname)).ToListAsync();

            return customers;
        }

        public async Task UpdateCustomer(Customer customer)
        {
            var customerToUpdate = await _dbContext.Customers.FirstOrDefaultAsync(customer => customer.Id == customer.Id);

            if (customerToUpdate != null)
            {
                customerToUpdate.Firstname = customer.Firstname;
                customerToUpdate.Lastname = customer.Lastname;
                await _dbContext.SaveChangesAsync();
            }
        }
    }
}
