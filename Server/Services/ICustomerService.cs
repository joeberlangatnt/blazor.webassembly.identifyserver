﻿using BlazorIdentity.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorIdentity.Server.Services
{
    public interface ICustomerService
    {
        public Task AddCustomer(Customer customer);

        public Task<Customer> GetCustomer(int id);

        public Task<IEnumerable<Customer>> GetCustomers();

        public Task DeleteCustomer(int id);

        public Task UpdateCustomer(Customer customer);

        public Task<IEnumerable<Customer>> Search(string searchTerm);
    }
}
