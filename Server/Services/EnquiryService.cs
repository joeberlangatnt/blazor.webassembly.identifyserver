﻿using BlazorIdentity.Server.Context;
using BlazorIdentity.Shared.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorIdentity.Server.Services
{
    public class EnquiryService : IEnquiryService
    {
        private readonly TestContext _dbContext;

        public EnquiryService(TestContext _dbContext)
        {
            this._dbContext = _dbContext;
        }

        public async Task AddEnquiry(Enquiry enquiry)
        {
            throw new NotImplementedException();
        }

        public async Task DeleteEnquiry(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Enquiry>> GetEnquiriesByCustomerId(int id)
        {
            //example to show join
            var query = await _dbContext.Customers.Include(e => e.Enquiries).FirstOrDefaultAsync(c => c.Id == id);

            var results = await _dbContext.Enquiries.Where(e => e.Customerid == id).ToListAsync();

            return results;
        }

        public async Task<Enquiry> GetEnquiry(int id)
        {
            throw new NotImplementedException();
        }

        public async Task UpdateEnquiry(Enquiry customer)
        {
            throw new NotImplementedException();
        }
    }
}
