﻿using BlazorIdentity.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlazorIdentity.Server.Services
{
    public interface IEnquiryService
    {
        public Task AddEnquiry(Enquiry enquiry);

        public Task<Enquiry> GetEnquiry(int id);

        public Task<IEnumerable<Enquiry>> GetEnquiriesByCustomerId(int id);

        public Task DeleteEnquiry(int id);

        public Task UpdateEnquiry(Enquiry customer);
    }
}
